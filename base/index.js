// primitive value : number, string, boolean, null, undefined

// reference value : array , object

//  array => [ ] => lưu cách dữ liệu cùng nhóm => index, phần tử
// object => { } => lưu các thông tin khác nhau mô tả cùng 1 đối tượng => key : value

// before : 1 học viên => 3 biến name, phone, age ~ 10 học viên => 30 biến khác nhau

// after : 10 object để lưu 10 học viên

// key : value, value data => thuộc tính, value function => phương thức

var dogName = "lulu";
var dogAge = 2;

// key : value

var dog1 = {
  name: "lulu",
  color: "black",
  speak: function () {
    console.log("gâu gâu tao là :", this.name);
    return "hello";
  },
};
console.log(dog1.speak());
var dog2 = {
  name: "mimi",
  age: 4,
  speak: function () {
    console.log("meo meo", this.name);
  },
};
// có 1 , ko 0
console.log(dog2.name);

// class : lớp đối tượng

var cat1 = {
  name: "meo meo",
};

var cat2 = {
  ten: "miu miu",
};

function Dog(ten, tuoi) {
  this.nickname = ten;
  this.age = tuoi;
  this.speak = function () {
    console.log("gau gau");
  };
}

var dog1 = new Dog("lulu", 2);

/**
 * var dog1 = {
 *    name:"lulu",
 *    age:2
 *    }
 *
 */
var dog2 = new Dog("mimi", 3);

console.log(dog1, dog2);
