var dssv = [];

// lấy dữ liệ từ localStorage lúc user load trang
var dataJson = localStorage.getItem("DSSV");
if (dataJson != null) {
  dssv = JSON.parse(dataJson).map(function (item) {
    return new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.toan,
      item.ly,
      item.hoa
    );
  });
  // map
  renderDSSV(dssv);
}

function themSinhVien() {
  // lấy thông tin từ form

  var sv = layThongTinTuForm();
  // validate

  // & cộng bit , 1+1 =1, 0+1 =0 , 1 true, 0 false

  // kt masv
  var isValid =
    kiemTraTrung(sv.ma, dssv) &&
    kiemTraDoDai(5, 5, "spanMaSV", "mã sv gồm 5 kí tự", sv.ma);
  // kt makhau
  isValid =
    isValid &
    kiemTraDoDai(
      4,
      8,
      "spanMatKhau",
      "Mật khẩu phải từ 4 đến 8 kí tự",
      sv.matKhau
    );
  // kt email
  isValid &= kiemTraEmail(sv.email);
  // maSv chỉ dc nhập 5 kí tự
  if (isValid) {
    dssv.push(sv);
    //   render dssv
    renderDSSV(dssv);
    //   save dssv localStorage
    // localStorage  : nơi lưu trữ ( chỉ chấp nhận json ) , json : 1 loại dữ liệu
    var dataJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV", dataJson);

    // reset => dùng thẻ form để reset thẻ input
    document.getElementById("formQLSV").reset();
  }
}

function xoaSinhVien(id) {
  console.log(id);
  // splice findIndex
  var index = dssv.findIndex(function (item) {
    console.log(" item", item);
    return item.ma == id;
  });
  dssv.splice(index, 1);
  renderDSSV(dssv);
}

function suaSinhVien(id) {
  // tìm vị trí sv trong dssv có mã trùng với id của onlick
  var index = dssv.findIndex(function (item) {
    return item.ma == id;
  });
  showThongTinLenFrom(dssv[index]);

  // show thong tin lên form
}

function capNhatSinhVien() {
  // lấy thong tin
}

function introduce(value, callback) {
  console.log("🚀 - file: main.js:60 - introduce - value", value);
  callback(value);
  // sayHello("alice")
}

// array map  js w3
// call back function

// mình
function sayHello(name) {
  console.log("heelo", name);
}
// tv

introduce("alice", function (name) {
  console.log("bai bai bai", name);
});

var nums = [2, 3, 4];
var newNums = nums.map(function (num) {
  return "ALICE";
});
console.log({ nums, newNums });
