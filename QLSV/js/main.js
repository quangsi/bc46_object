function hienThiThongTin() {
  //   lấy dữ liệu từ form
  var result = layThongTinTuForm();
  //   tạo object gồm 7 key
  var sv = {
    maSv: result.ma,
    tenSv: result.ten,
    loaiSv: result.loai,
    diemToan: result.toan,
    diemVan: result.van,
    tinhDTB: function () {
      var dtb = (this.diemToan + this.diemVan) / 2;
      return dtb;
    },
    xepLoai: function () {
      // >5 => đạt
      if (this.tinhDTB() >= 5) {
        return "Đạt";
      } else {
        return "Rớt";
      }
    },
  };

  //   show thông tin lên form
  showThongTinLenForm(sv);
}

// gắn sự kiện cho button bằng addEventListener
document
  .getElementById("btn-hien-thi")
  .addEventListener("click", hienThiThongTin);

console.log(username);
